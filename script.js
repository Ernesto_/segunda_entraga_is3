document
  .getElementById("loginForm")
  .addEventListener("submit", function (event) {
    event.preventDefault();

    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;

    if (username === "" || password === "") {
      document.getElementById("error-message").innerText =
        "Please fill out all fields.";
      document.getElementById("error-message").style.display = "block";
    } else {
      document.getElementById("error-message").style.display = "none";
      // Aquí podrías enviar los datos a tu servidor PHP para procesar el inicio de sesión.
      // Por ejemplo, usando fetch para hacer una petición AJAX.
      window.location.href = "menu.html";
    }
  });
